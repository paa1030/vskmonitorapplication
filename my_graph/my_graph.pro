#
#  QCustomPlot Plot Examples
#

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

greaterThan(QT_MAJOR_VERSION, 4): CONFIG += c++98
lessThan(QT_MAJOR_VERSION, 5): QMAKE_CXXFLAGS += -std=c++98

TARGET = my_graph
TEMPLATE = app

SOURCES += main.cpp\
           mainwindow.cpp \
           qcustomplot.cpp

HEADERS  += mainwindow.h \
            qcustomplot.h \

FORMS    += mainwindow.ui


unix:!macx: LIBS += -L$$PWD/lib/ -lvsk_nkbvs -ljansson

INCLUDEPATH += $$PWD/lib
DEPENDPATH += $$PWD/lib

unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libvsk_nkbvs.a
