/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011-2021 Emanuel Eichhammer                            **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Emanuel Eichhammer                                   **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 29.03.21                                             **
**          Version: 2.1.0                                                **
****************************************************************************/

/************************************************************************************************************
**                                                                                                         **
**  This is the example code for QCustomPlot.                                                              **
**                                                                                                         **
**  It demonstrates basic and some advanced capabilities of the widget. The interesting code is inside     **
**  the "setup(...)Demo" functions of MainWindow.                                                          **
**                                                                                                         **
**  In order to see a demo in action, call the respective "setup(...)Demo" function inside the             **
**  MainWindow constructor. Alternatively you may call setupDemo(i) where i is the index of the demo       **
**  you want (for those, see MainWindow constructor comments). All other functions here are merely a       **
**  way to easily create screenshots of all demos for the website. I.e. a timer is set to successively     **
**  setup all the demos and make a screenshot of the window area and save it in the ./screenshots          **
**  directory.                                                                                             **
**                                                                                                         **
*************************************************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#  include <QDesktopWidget>
#endif
#include <QScreen>
#include <QMessageBox>
#include <QMetaEnum>

extern "C"
{
#include "lib/vsk_nkbvs_lib.h"
}


int sens_count = 0;
sensor_graph_t *sensors;

LIST_HEAD(request_list);

module_t *mptr;
object_of_control_t *sptr;

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  setupRealtimeDataDemo(ui->customPlot);
  setWindowTitle("QCustomPlot: "+demoName);
  statusBar()->clearMessage();
  ui->customPlot->replot();
}

void MainWindow::setupRealtimeDataDemo(QCustomPlot *customPlot)
{
  demoName = "Real Time Data Demo";
  char name[100];

  
  // include this section to fully disable antialiasing for higher performance:
/*
  customPlot->setNotAntialiasedElements(QCP::aeAll);
  QFont font;
  font.setStyleStrategy(QFont::NoAntialias);
  customPlot->xAxis->setTickLabelFont(font);
  customPlot->yAxis->setTickLabelFont(font);
  customPlot->legend->setFont(font);
*/
  updateData();

  list_for_each_entry(mptr,  &request_list, list) {
    if (mptr->module_of_control != NULL) {
      sens_count++;
      list_for_each_entry(sptr, &mptr->object_list, list) {
        if (NULL != sptr->object.name) {
          sens_count++;
        }
      }
    }
  }

  sensors = new sensor_graph_t[sens_count];

//  customPlot->addGraph(); // blue line
//  customPlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
//  customPlot->addGraph(); // red line
//  customPlot->graph(1)->setPen(QPen(QColor(255, 110, 40)));

  QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
  timeTicker->setTimeFormat("%h:%m:%s");

  QCPMarginGroup *group = new QCPMarginGroup(customPlot);
  customPlot->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, group);

  customPlot->plotLayout()->insertRow(0);
  customPlot->plotLayout()->addElement(0, 0, new QCPTextElement(customPlot, "System State", QFont("sans", 12, QFont::Bold)));
  int i = 0;
      list_for_each_entry(mptr,  &request_list, list) {
        if (mptr->module_of_control != NULL) {
          sensors[i].mod_name = mptr->module_of_control;
          sensors[i].sens_name = NULL;

          customPlot->plotLayout()->insertRow((i*2)+2);
          customPlot->plotLayout()->addElement((i*2)+2, 0, new QCPTextElement(customPlot, sensors[i].mod_name, QFont("sans", 12, QFont::Bold)));
          sensors[i].Element = new QCPAxisRect(customPlot);
          customPlot->plotLayout()->addElement((i*2)+3, 0, sensors[i].Element);
          sensors[i].Element->setMinimumSize(QSize(150, 150));

          sensors[i].Max_err_val = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
          sensors[i].Max_err_val->setPen(QPen(QColor(255,0,0)));
          sensors[i].Min_err_val = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
          sensors[i].Min_err_val->setPen(QPen(QColor(255,0,0)));
          sensors[i].Max_var_val = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
          sensors[i].Max_var_val->setPen(QPen(QColor(255,165,0)));
          sensors[i].Min_var_val = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
          sensors[i].Min_var_val->setPen(QPen(QColor(255,165,0)));
          sensors[i].Minus_odin = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
          sensors[i].Minus_odin->setPen(QPen(QColor(255,255,255)));
          sensors[i].Plus_odin = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
          sensors[i].Plus_odin->setPen(QPen(QColor(255,255,255)));
          sensors[i].Graph   = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
          sensors[i].Graph->setPen(QPen(QColor(0,0,255)));

          sensors[i].Element->axis(QCPAxis::atBottom)->setTicker(timeTicker);

          connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), sensors[i].Element->axis(QCPAxis::atBottom), SLOT(setRange(QCPRange)));
          connect(sensors[i].Element->axis(QCPAxis::atBottom), SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis, SLOT(setRange(QCPRange)));

          sensors[i].Element->setMarginGroup(QCP::msLeft|QCP::msRight, group);
          i++;
          list_for_each_entry(sptr, &mptr->object_list, list) {
            if (NULL != sptr->object.name) {
              sensors[i].mod_name = mptr->module_of_control;
              sensors[i].sens_name = sptr->object.name;
              sprintf(name, "%s::%s", sensors[i].mod_name, sensors[i].sens_name);
              customPlot->plotLayout()->insertRow((i*2)+2);
              customPlot->plotLayout()->addElement((i*2)+2, 0, new QCPTextElement(customPlot, name, QFont("sans", 12, QFont::Bold)));
              sensors[i].Element = new QCPAxisRect(customPlot);
              customPlot->plotLayout()->addElement((i*2)+3, 0, sensors[i].Element);
              sensors[i].Element->setMinimumSize(QSize(150, 150));

              sensors[i].Minus_odin = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
              sensors[i].Minus_odin->setPen(QPen(QColor(255,255,255)));
              sensors[i].Plus_odin = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
              sensors[i].Plus_odin->setPen(QPen(QColor(255,255,255)));
              sensors[i].Max_err_val = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
              sensors[i].Max_err_val->setPen(QPen(QColor(255,0,0)));
              sensors[i].Min_err_val = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
              sensors[i].Min_err_val->setPen(QPen(QColor(255,0,0)));
              sensors[i].Max_var_val = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
              sensors[i].Max_var_val->setPen(QPen(QColor(255,165,0)));
              sensors[i].Min_var_val = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
              sensors[i].Min_var_val->setPen(QPen(QColor(255,165,0)));
              sensors[i].Graph   = new QCPGraph(sensors[i].Element->axis(QCPAxis::atBottom), sensors[i].Element->axis(QCPAxis::atLeft));
              sensors[i].Graph->setPen(QPen(QColor(0,0,255)));

              sensors[i].Element->axis(QCPAxis::atBottom)->setTicker(timeTicker);

              connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), sensors[i].Element->axis(QCPAxis::atBottom), SLOT(setRange(QCPRange)));
              connect(sensors[i].Element->axis(QCPAxis::atBottom), SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis, SLOT(setRange(QCPRange)));

              sensors[i].Element->setMarginGroup(QCP::msLeft|QCP::msRight, group);
              i++;
            }
          }
        }
      }

//  customPlot->xAxis->setTicker(timeTicker);
//  customPlot->axisRect()->setupFullAxesBox();
//  customPlot->yAxis->setRange(-1.2, 1.2);
  customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
  
  // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
  connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
  dataTimer.start(1000); // Interval 0 means to refresh as fast as possible
}

void MainWindow::realtimeDataSlot()
{
  static QTime timeStart = QTime::currentTime();
  // calculate two new data points:
  double key = timeStart.msecsTo(QTime::currentTime())/1000.0; // time elapsed since start of demo, in seconds
  static double lastPointKey = 0;

    updateData();

    list_for_each_entry(mptr,  &request_list, list) {
    if (mptr->module_of_control != NULL) {
      for (int i=0; i<sens_count; ++i)
      {
        if((sensors[i].mod_name == mptr->module_of_control) && (sensors[i].sens_name == NULL))
        {
          sensors[i].Graph->addData(key, mptr->object_status);
          sensors[i].Max_err_val->addData(key, 1.5);
          sensors[i].Max_var_val->addData(key, 0.5);
          sensors[i].Plus_odin->addData(key, (mptr->object_status +1));
          sensors[i].Minus_odin->addData(key, (mptr->object_status -1));

          sensors[i].Plus_odin->rescaleValueAxis(true);
          sensors[i].Minus_odin->rescaleValueAxis(true);
          sensors[i].Graph->rescaleValueAxis(true);

          break;
        }
      }

      list_for_each_entry(sptr, &mptr->object_list, list) {
      if (NULL != sptr->object.name) {
          for (int i=0; i<sens_count; ++i)
          {
            if ((sensors[i].mod_name == mptr->module_of_control) && (sensors[i].sens_name == sptr->object.name))
            {
              if (sptr->object.val_type == VAL_TYPE_INT) {
                sensors[i].Graph->addData(key, sptr->object.val.int_val.cur_val);
                sensors[i].Max_err_val->addData(key, sptr->object.val.int_val.max_val);
                sensors[i].Min_err_val->addData(key, sptr->object.val.int_val.min_val);
                sensors[i].Max_var_val->addData(key, (sptr->object.val.int_val.max_val * 0.85));
                sensors[i].Min_var_val->addData(key, (sptr->object.val.int_val.min_val * 1.15));
                sensors[i].Plus_odin->addData(key, (sptr->object.val.int_val.cur_val + 1));
                sensors[i].Minus_odin->addData(key, (sptr->object.val.int_val.cur_val - 1));

                sensors[i].Plus_odin->rescaleValueAxis(true);
                sensors[i].Minus_odin->rescaleValueAxis(true);
                sensors[i].Graph->rescaleValueAxis(true);
              }

              if (sptr->object.val_type == VAL_TYPE_FLOAT) {
                sensors[i].Graph->addData(key, sptr->object.val.float_val.cur_val);
                sensors[i].Max_err_val->addData(key, sptr->object.val.float_val.max_val);
                sensors[i].Min_err_val->addData(key, sptr->object.val.float_val.min_val);
                sensors[i].Max_var_val->addData(key, (sptr->object.val.float_val.max_val * 0.85));
                sensors[i].Min_var_val->addData(key, (sptr->object.val.float_val.min_val * 1.15));
                sensors[i].Plus_odin->addData(key, (sptr->object.val.float_val.cur_val + 1));
                sensors[i].Minus_odin->addData(key, (sptr->object.val.float_val.cur_val - 1));

                sensors[i].Plus_odin->rescaleValueAxis(true);
                sensors[i].Minus_odin->rescaleValueAxis(true);
                sensors[i].Graph->rescaleValueAxis(true);

              }

              break;
            }
          }
       }
     }
   }
 }

    // add data to lines:
//    ui->customPlot->graph(0)->addData(key, qSin(key)+std::rand()/(double)RAND_MAX*1*qSin(key/0.3843));
//    ui->customPlot->graph(1)->addData(key, qCos(key)+std::rand()/(double)RAND_MAX*0.5*qSin(key/0.4364));
    // rescale value (vertical) axis to fit the current data:
//    ui->customPlot->graph(0)->rescaleValueAxis(true);
//    ui->customPlot->graph(1)->rescaleValueAxis(true);
  lastPointKey = key;

  ui->customPlot->replot();

}

void MainWindow::updateData()
{
    /*
     * В этом примере мы получаем от ВСК информацию о всех объектах, которые проверяются
     * в ходе периодических проверок. Сохраняем эти данные в список структур request_list,
     * и далее выводим в консоль информацию о каждом объекте.
     */
    int32_t vsk_fd = -1;

    vsk_fd = lib_vsk_connect_vsk("127.0.0.1", 5000);
    if (vsk_fd >= 0) {
        lib_vsk_get_info(vsk_fd, &request_list);
        lib_vsk_disconnect_vsk(vsk_fd);
    }

}



MainWindow::~MainWindow()
{
  delete ui;
}






































